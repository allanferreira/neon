const gulp       = require('gulp'),
    pug          = require('gulp-pug'),
    stylus       = require('gulp-stylus'),
    babel        = require('gulp-babel'),
    autoprefixer = require('gulp-autoprefixer'),
    plumber      = require('gulp-plumber'),
    imagemin     = require('gulp-imagemin'),
    browserSync  = require('browser-sync').create()

gulp.task('stylus', () => {
    return gulp.src('src/css/main.styl')
        .pipe(plumber())
        .pipe(stylus({
            compress: true
        }))
        .pipe(autoprefixer({
            browsers: ['last 2 versions'],
            cascade: false
        }))
        .pipe(plumber.stop())
        .pipe(gulp.dest('./dist/css'))
})

 
gulp.task('babel', () => {
    return gulp.src('src/js/**/*.js')
        .pipe(plumber())
        .pipe(babel({
            presets: ['es2015']
        }))
        .pipe(plumber.stop())
        .pipe(gulp.dest('dist/js'))
})
 
gulp.task('pug', function buildHTML() {
    return gulp.src('src/**/*.pug')
        .pipe(plumber())
        .pipe(pug({
            pretty: true
        }))
        .pipe(plumber.stop())
        .pipe(gulp.dest('./dist'))
});

gulp.task('imagemin', () => {
    return gulp.src('src/img/*')
        .pipe(imagemin())
        .pipe(gulp.dest('./dist/img'))
})

gulp.task('server', () => {
    var files = [
        './dist/**/*'
    ]
    browserSync.init( files, {
        server: {
            baseDir: "./dist"
        }
    })
})

gulp.task('watch', () => {
    gulp.watch('src/**/*.pug', ['pug'])
    gulp.watch('src/js/**/*.js', ['babel'])
    gulp.watch('src/css/**/*.styl', ['stylus'])
    gulp.watch('src/img/*', ['imagemin'])
})

gulp.task('build', ['pug', 'stylus', 'babel', 'imagemin'])
gulp.task('default', ['build'])