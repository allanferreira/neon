# NEON
> http://mechamoallan.com.br/neon

This project just use pure CSS n' JS. No libs in client man!

## Getting Started
Open your favorite Terminal and run these commands.

### Installation
Install dependences globally if necessary:
```bash
$ npm install --global gulp-cli
$ npm install -g browser-sync
```

### Run app
Build files and start the server
```sh
$ npm install
$ gulp build && gulp server
```