let sliderIndex = 1
slider(sliderIndex)

function passSlide(n) {
	slider(sliderIndex += n)
}

function slider(n) {
	const items = document.getElementsByClassName("slider__content__item")
	sliderIndex = (n > items.length) ? 1 : (n < 1) ? items.length : sliderIndex

	const arrowRightActive = document.querySelector('.slider__nav--right.icon--arrow-simple--right--active'),
		arrowRight       = document.querySelector('.slider__nav--right.icon--arrow-simple--right--border'),
		arrowLeftActive  = document.querySelector('.slider__nav--left.icon--arrow-simple--left--active'),
		arrowLeft        = document.querySelector('.slider__nav--left.icon--arrow-simple--left--border')

	for (var i = 0; i < items.length; i++) {
		items[i].style.display = "none"
	}
	items[sliderIndex-1].style.display = "block"

	if(sliderIndex == 1){
		if(items.length == 1){
			arrowRightActive.style.display = "none"
			arrowRight.style.display = "block"
			arrowLeftActive.style.display = "none"
			arrowLeft.style.display = "block"
		}
		else{
			arrowRightActive.style.display = "block"
			arrowRight.style.display = "none"
			arrowLeftActive.style.display = "none"
			arrowLeft.style.display = "block"
		}
	} else if(sliderIndex == items.length){
		arrowRightActive.style.display = "none"
		arrowRight.style.display = "block"
		arrowLeftActive.style.display = "block"
		arrowLeft.style.display = "none"
	} else{
		arrowRightActive.style.display = "block"
		arrowRight.style.display = "none"
		arrowLeftActive.style.display = "block"
		arrowLeft.style.display = "none"
	}
}
